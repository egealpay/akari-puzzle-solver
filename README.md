# Akari #

![alt text](akari_board_ss.png)

## Definition of the game ##

Akari uses a rectangular grid of black cells and white cells.1 The player solves puzzles via placing light bulbs in the white boxes according to following rules:
• Light bulbs are permitted to be placed at any white square. A numbered square indicates how many light bulbs are next to it, vertically and horizontally.
• Each light bulb illuminates from itself to a black square or the outer frame in its row and column.
• Every white square must be illuminated and no light bulbs should illuminate each other.

Board is represented as a matrix:
```
[
    ["()", "()", "()", 1, 1, "()", "()"],
    ["()", "X", "()", "()", "()", "X", "()"],
    ["X", "()", "()", "()", "()", "()", "()"],
    ["X", "()", "()", "()", "()", "()", 1],
    ["()", "()", "()", "()", "()", "()", "X"],
    ["()", 3, "()", "()", "()", 0, "()"],
    ["()", "()", 1, 0, "()", "()", "()"]
]
```

In this project, I have modeled this game as a Constraint Satisfaction Model. I have used python-constraint module which you can find it 
from [here](http://labix.org/python-constraint/)

### Project Reports ###
[Report](https://gitlab.com/egealpay/akari-puzzle-solver/blob/master/akari_report.pdf)